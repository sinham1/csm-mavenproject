package com.coverity.security;

import java.util.regex.Pattern;

public class TestClassString {
	private final static Pattern SCHEME_REGEX = Pattern.compile("(javascript|vbscript|data|about)");

	public static void main(String[] args) {
		boolean conditionFlag = false;
		final String returnValue = "<script>alert('Click');</script>";
		String schemaRegenString = SCHEME_REGEX.pattern().toString().substring(1,
				SCHEME_REGEX.pattern().toString().length() - 1);
		String[] schemaRegenArray = schemaRegenString.split("\\|");
		for (String a : schemaRegenArray) {
			if (returnValue.contains(a)) {
				conditionFlag = true;
				break;
			}

		}
		if (conditionFlag) {
			String values = returnValue.replaceAll(SCHEME_REGEX.pattern().toString(), "");
			System.out.println("Condition Value===============> "+values);
		}

	}

}
